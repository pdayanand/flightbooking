package org.flightbooking.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.flightbooking.models.*;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import static org.junit.Assert.*;

import static org.junit.Assert.assertArrayEquals;


import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FlightServiceTest {

    private void setupTestData(ArrayList<Trip> trips, int days) throws ParseException {

        Route r = new Route( 1, "Bangalore", "Hyderabad");

        Flight f = new Flight( 1,
                "IN 1",
                "Indigo",
                AirPlaneModel.BOEING_777_200LR,
                195,
                35,
                20);

        TravelClassFare boeing777FC = new TravelClassFare();
        boeing777FC.setTravelClass(TravelClass.FirstClass);

        boeing777FC.setRemainingSeats(8);
        boeing777FC.setBasePrice(20000);

        TravelClassFare boeing777BC = new TravelClassFare();
        boeing777BC.setTravelClass(TravelClass.BusinessClass);

        boeing777BC.setRemainingSeats(10);
        boeing777BC.setBasePrice(13000);

        TravelClassFare boeing777EC = new TravelClassFare();
        boeing777EC.setTravelClass(TravelClass.EconomyClass);

        boeing777EC.setRemainingSeats(195);
        boeing777EC.setBasePrice(6000);

        //Set up trips for three days
        for(int i = 0; i< days ; i++ ) {
            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();

            switch (i%3) {
                case 0:
                    travelClassFares.put(TravelClass.FirstClass, boeing777FC);
                    break;
                case 1:
                    travelClassFares.put(TravelClass.BusinessClass, boeing777BC);
                    break;
                case 2:
                    travelClassFares.put(TravelClass.EconomyClass, boeing777EC);
                    break;
            }

            Trip trip = new Trip();
            trip.setTripId(i);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2019"));
            c.add(Calendar.DAY_OF_WEEK, i);
            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }
    }

    private void setupResultSet(ArrayList<TripResultSet> resultSets) {
        TripResultSet result = new TripResultSet();
        result.setTripId(0);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("15/08/2019");
        result.setDeparture("14:15");
        result.setArrival("16:45");

        result.addFare(TravelClass.FirstClass, 40000);

        resultSets.add(result);

        result = new TripResultSet();
        result.setTripId(1);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("16/08/2019");
        result.setDeparture("14:15");
        result.setArrival("16:45");
        result.addFare(TravelClass.BusinessClass, 36400);
        resultSets.add(result);

        result = new TripResultSet();
        result.setTripId(2);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("17/08/2019");
        result.setDeparture("14:15");
        result.setArrival("16:45");

        result.addFare(TravelClass.EconomyClass, 12000);
        resultSets.add(result);
    }

    private void setupResultSetWithDate(ArrayList<TripResultSet> resultSets) {
        TripResultSet result = new TripResultSet();
        result.setTripId(0);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("15/08/2019");
        result.setDeparture("14:15");
        result.setArrival("16:45");

        result.addFare(TravelClass.FirstClass, 0);

        resultSets.add(result);

    }


    private void setupResultSetWithClass(ArrayList<TripResultSet> resultSets)
    {
        setupResultSetWithDate(resultSets);
    }

    private void setupTestDataForEconomyFareCalc(ArrayList<Trip> trips) throws ParseException {

        Route r = new Route(1, "Bangalore", "Hyderabad");

        Flight f = new Flight(1,
                "IN 1",
                "Indigo",
                AirPlaneModel.BOEING_777_200LR,
                195,
                35,
                20);


        {

            TravelClassFare boeing777EC = new TravelClassFare();
            boeing777EC.setTravelClass(TravelClass.EconomyClass);
            boeing777EC.setRemainingSeats(150);
            boeing777EC.setBasePrice(6000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.EconomyClass, boeing777EC);

            Trip trip = new Trip();
            trip.setTripId(1);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {

            TravelClassFare boeing777EC = new TravelClassFare();
            boeing777EC.setTravelClass(TravelClass.EconomyClass);
            boeing777EC.setRemainingSeats(90);
            boeing777EC.setBasePrice(6000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.EconomyClass, boeing777EC);

            Trip trip = new Trip();
            trip.setTripId(2);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 10);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {
            TravelClassFare boeing777EC = new TravelClassFare();
            boeing777EC.setTravelClass(TravelClass.EconomyClass);
            boeing777EC.setRemainingSeats(5);
            boeing777EC.setBasePrice(6000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.EconomyClass, boeing777EC);

            Trip trip = new Trip();
            trip.setTripId(3);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

    }

    private void setupResultSetForEconomyFareCalc(ArrayList<TripResultSet> resultSets) {
        TripResultSet result = new TripResultSet();
        result.setTripId(1);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("15/08/2019");
        result.setDeparture("14:15");
        result.setArrival("16:45");

        result.addFare(TravelClass.EconomyClass, 6000);

        resultSets.add(result);

        result = new TripResultSet();
        result.setTripId(2);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("15/08/2019");
        result.setDeparture("10:15");
        result.setArrival("12:45");
        result.addFare(TravelClass.EconomyClass, 7800);
        resultSets.add(result);

        result = new TripResultSet();
        result.setTripId(3);
        result.setOrigin("Bangalore");
        result.setDestination("Hyderabad");
        result.setName("IN 1");
        result.setCarrier("Indigo");
        result.setModel(AirPlaneModel.BOEING_777_200LR);

        result.setTravelDate("15/08/2019");
        result.setDeparture("18:15");
        result.setArrival("20:45");

        result.addFare(TravelClass.EconomyClass, 9600);
        resultSets.add(result);
    }

    private void setupTestDataForBusinessFareCalc(ArrayList<Trip> trips) throws ParseException {

        Route r = new Route(1, "Bangalore", "Hyderabad");

        Flight f = new Flight(1,
                "IN 1",
                "Indigo",
                AirPlaneModel.BOEING_777_200LR,
                195,
                35,
                20);

        {

            TravelClassFare boeing777BC = new TravelClassFare();
            boeing777BC.setTravelClass(TravelClass.BusinessClass);
            boeing777BC.setRemainingSeats(35);
            boeing777BC.setBasePrice(13000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.BusinessClass, boeing777BC);

            Trip trip = new Trip();
            trip.setTripId(1);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("15/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {

            TravelClassFare boeing777BC = new TravelClassFare();
            boeing777BC.setTravelClass(TravelClass.BusinessClass);
            boeing777BC.setRemainingSeats(35);
            boeing777BC.setBasePrice(13000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.BusinessClass, boeing777BC);

            Trip trip = new Trip();
            trip.setTripId(2);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("16/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 10);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {
            TravelClassFare boeing777BC = new TravelClassFare();
            boeing777BC.setTravelClass(TravelClass.BusinessClass);
            boeing777BC.setRemainingSeats(35);
            boeing777BC.setBasePrice(13000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.BusinessClass, boeing777BC);

            Trip trip = new Trip();
            trip.setTripId(3);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("18/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

       {
            TravelClassFare boeing777BC = new TravelClassFare();
            boeing777BC.setTravelClass(TravelClass.BusinessClass);
            boeing777BC.setRemainingSeats(35);
            boeing777BC.setBasePrice(13000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.BusinessClass, boeing777BC);

            Trip trip = new Trip();
            trip.setTripId(4);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("dd/MM/yyyy").parse("19/08/2019"));

            c.set(Calendar.HOUR_OF_DAY, 6);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

    }

    private void setupResultSetForBusinessFareCalc(ArrayList<TripResultSet> resultSets) {
        {
            TripResultSet result = new TripResultSet();

            result.setTripId(1);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate("15/08/2019");
            result.setDeparture("14:15");
            result.setArrival("16:45");

            result.addFare(TravelClass.BusinessClass, 13000);

            resultSets.add(result);
        }
       {
            TripResultSet result = new TripResultSet();


            result = new TripResultSet();
            result.setTripId(2);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate("16/08/2019");
            result.setDeparture("10:15");
            result.setArrival("12:45");
            result.addFare(TravelClass.BusinessClass, 18200);
            resultSets.add(result);
        }

        {
            TripResultSet result = new TripResultSet();
            result = new TripResultSet();
            result.setTripId(3);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate("18/08/2019");
            result.setDeparture("18:15");
            result.setArrival("20:45");

            result.addFare(TravelClass.BusinessClass, 18200);
            resultSets.add(result);
        }

        {
            TripResultSet result = new TripResultSet();
            result = new TripResultSet();
            result.setTripId(4);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate("19/08/2019");
            result.setDeparture("06:15");
            result.setArrival("08:45");

            result.addFare(TravelClass.BusinessClass, 18200);
            resultSets.add(result);
        }
    }

    private void setupTestDataForFirstFareCalc(ArrayList<Trip> trips) throws ParseException {

        Route r = new Route(1, "Bangalore", "Hyderabad");

        Flight f = new Flight(1,
                "IN 1",
                "Indigo",
                AirPlaneModel.BOEING_777_200LR,
                195,
                35,
                20);

        Date today = Calendar.getInstance().getTime();

        {

            TravelClassFare boeing777FC = new TravelClassFare();
            boeing777FC.setTravelClass(TravelClass.FirstClass);
            boeing777FC.setRemainingSeats(10);
            boeing777FC.setBasePrice(20000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.FirstClass, boeing777FC);

            Trip trip = new Trip();
            trip.setTripId(1);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(today);

            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {

            TravelClassFare boeing777FC = new TravelClassFare();
            boeing777FC.setTravelClass(TravelClass.FirstClass);
            boeing777FC.setRemainingSeats(10);
            boeing777FC.setBasePrice(20000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.FirstClass, boeing777FC);

            Trip trip = new Trip();
            trip.setTripId(1);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(today);

            c.set(Calendar.HOUR_OF_DAY, 14);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            c.add(Calendar.DAY_OF_YEAR, 1);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {

            TravelClassFare boeing777FC = new TravelClassFare();
            boeing777FC.setTravelClass(TravelClass.FirstClass);
            boeing777FC.setRemainingSeats(10);
            boeing777FC.setBasePrice(20000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.FirstClass, boeing777FC);

            Trip trip = new Trip();
            trip.setTripId(2);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(today);

            c.set(Calendar.HOUR_OF_DAY, 10);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            c.add(Calendar.DAY_OF_YEAR, 3);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

        {
            TravelClassFare boeing777FC = new TravelClassFare();
            boeing777FC.setTravelClass(TravelClass.FirstClass);
            boeing777FC.setRemainingSeats(10);
            boeing777FC.setBasePrice(20000);

            Map<TravelClass, TravelClassFare> travelClassFares = new HashMap<>();
            travelClassFares.put(TravelClass.FirstClass, boeing777FC);

            Trip trip = new Trip();
            trip.setTripId(3);
            trip.setFlight(f);
            trip.setTripRoute(r);
            trip.setFlightLength(Duration.parse("P0DT2H30M30S"));
            Calendar c = Calendar.getInstance();
            c.setTime(today);

            c.set(Calendar.HOUR_OF_DAY, 18);
            c.set(Calendar.MINUTE, 15);
            c.set(Calendar.SECOND, 0);
            c.add(Calendar.DAY_OF_YEAR, 10);
            trip.setFareDetails(travelClassFares);
            trip.setTravelDate(c.getTime());

            trips.add(trip);
        }

    }

    private void setupResultSetForFirstFareCalc(ArrayList<TripResultSet> resultSets) {
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());

        {
            TripResultSet result = new TripResultSet();


            result.setTripId(1);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate( new SimpleDateFormat("dd/MM/yyyy").format(today.getTime()));
            result.setDeparture("14:15");
            result.setArrival("16:45");

            result.addFare(TravelClass.FirstClass, 40000);

            resultSets.add(result);
        }

        {
            today.add(Calendar.DAY_OF_YEAR, 1);
            TripResultSet result = new TripResultSet();


            result.setTripId(1);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate( new SimpleDateFormat("dd/MM/yyyy").format(today.getTime()));
            result.setDeparture("14:15");
            result.setArrival("16:45");

            result.addFare(TravelClass.FirstClass, 38000);

            resultSets.add(result);
        }
        {
            TripResultSet result = new TripResultSet();
            today.add(Calendar.DAY_OF_YEAR, 2);

            result = new TripResultSet();
            result.setTripId(2);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate(new SimpleDateFormat("dd/MM/yyyy").format(today.getTime()));
            result.setDeparture("10:15");
            result.setArrival("12:45");
            result.addFare(TravelClass.FirstClass, 34000);
            resultSets.add(result);
        }

        {

            today.add(Calendar.DAY_OF_YEAR, 7);
            TripResultSet result = new TripResultSet();
            result = new TripResultSet();
            result.setTripId(3);
            result.setOrigin("Bangalore");
            result.setDestination("Hyderabad");
            result.setName("IN 1");
            result.setCarrier("Indigo");
            result.setModel(AirPlaneModel.BOEING_777_200LR);

            result.setTravelDate(new SimpleDateFormat("dd/MM/yyyy").format(today.getTime()));
            result.setDeparture("18:15");
            result.setArrival("20:45");

            result.addFare(TravelClass.FirstClass, 20000);
            resultSets.add(result);
        }

    }


    @Test
    public void searchFlightsWithOriginDestAndSeats() throws ParseException {

        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestData(trips, 3);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 2)).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(2);


        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSet(resultSets);

        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);
        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i), "travelClassFareMapping").matches(resultFromService.get(i))));
        }

    }


    @Test
    public void searchFlightsWithDate() throws ParseException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestData(trips, 1);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 2, LocalDate.parse("2019-08-15"))).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(2);
        search.setTravelDate(LocalDate.parse("2019-08-15"));


        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSetWithDate(resultSets);



        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);
        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i), "travelClassFareMapping").matches(resultFromService.get(i))));
        }


    }

    @Test
    public void searchFlightsWithClass() throws ParseException, JsonProcessingException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestData(trips, 1);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 2, LocalDate.parse("2019-08-15"), TravelClass.FirstClass)).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(2);
        search.setTravelDate(LocalDate.parse("2019-08-15"));
        search.setTravelClass(TravelClass.FirstClass);

        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSetWithClass(resultSets);



        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);
        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i), "travelClassFareMapping").matches(resultFromService.get(i))));
        }

    }

    @Test
    public void testFlightFare() throws ParseException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestData(trips, 3);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 2)).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(2);

        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSet(resultSets);

        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);

        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i)).matches(resultFromService.get(i))));
        }


    }

    @Test
    public void testFlightFareEconomyClass() throws ParseException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestDataForEconomyFareCalc(trips);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 1)).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(1);

        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSetForEconomyFareCalc(resultSets);


        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);

        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i)).matches(resultFromService.get(i))));
        }


    }

    @Test
    public void testFlightFareBusinessClass() throws ParseException, JsonProcessingException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestDataForBusinessFareCalc(trips);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 1)).thenReturn(trips);

        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(1);

        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSetForBusinessFareCalc(resultSets);


        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);

        for(int i =0;i <resultFromService.size();i++ ){
            Assert.assertTrue( (new ReflectionEquals(resultSets.get(i)).matches(resultFromService.get(i))));
        }


    }

    @Test
    public void testFlightFareFirstClass() throws ParseException, JsonProcessingException {
        //Test for flight lookup with origin, destination and no.of seats as input
        ArrayList<Trip> trips = new ArrayList<>();
        setupTestDataForFirstFareCalc(trips);

        FlightRepository mockRepo = mock(FlightRepository.class);
        when(mockRepo.search("Bangalore", "Hyderabad", 1, LocalDate.now(), TravelClass.FirstClass)).thenReturn(new ArrayList<Trip>(Arrays.asList(trips.get(0))));
        when(mockRepo.search("Bangalore", "Hyderabad", 1, LocalDate.now().plusDays(1), TravelClass.FirstClass)).thenReturn(new ArrayList<Trip>(Arrays.asList(trips.get(1))));
        when(mockRepo.search("Bangalore", "Hyderabad", 1, LocalDate.now().plusDays(3), TravelClass.FirstClass)).thenReturn(new ArrayList<Trip>(Arrays.asList(trips.get(2))));
        when(mockRepo.search("Bangalore", "Hyderabad", 1, LocalDate.now().plusDays(10), TravelClass.FirstClass)).thenReturn(new ArrayList<Trip>(Arrays.asList(trips.get(3))));


        FlightService service = new FlightService(mockRepo);

        FlightSearch search = new FlightSearch();
        search.setOrigin("Bangalore");
        search.setDestination("Hyderabad");
        search.setNumberOfSeats(1);
        search.setTravelDate(LocalDate.now());
        search.setTravelClass(TravelClass.FirstClass);

        ArrayList<TripResultSet> resultSets = new ArrayList<>();
        setupResultSetForFirstFareCalc(resultSets);

        ArrayList<TripResultSet> resultFromService = service.searchFlights(search);
        Assert.assertTrue( (new ReflectionEquals(resultSets.get(0)).matches(resultFromService.get(0))));


        search.setTravelDate(LocalDate.now().plusDays(1));
        resultFromService = service.searchFlights(search);
        Assert.assertTrue( (new ReflectionEquals(resultSets.get(1)).matches(resultFromService.get(0))));


        search.setTravelDate(LocalDate.now().plusDays(3));
        resultFromService = service.searchFlights(search);
        Assert.assertTrue( (new ReflectionEquals(resultSets.get(2)).matches(resultFromService.get(0))));


        search.setTravelDate(LocalDate.now().plusDays(10));
        resultFromService = service.searchFlights(search);
        Assert.assertTrue( (new ReflectionEquals(resultSets.get(3)).matches(resultFromService.get(0))));


    }



}