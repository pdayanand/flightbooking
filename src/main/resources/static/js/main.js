$("#flight-booking-form").submit(function (event) {
    //stop submit the form event. Do this manually using ajax post function
    event.preventDefault();
    var flightBookingForm = {};
    flightBookingForm["origin"] = $("#origin").val();
    flightBookingForm["destination"] = $("#dest").val();
    flightBookingForm["numberOfSeats"] = $("#seats").val();
    flightBookingForm["travelDate"] = $("#travel-date").val();
    flightBookingForm["travelClass"] = $("#travel-class").val();
    $("#btn-search").prop("disabled", true);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/search",
        data: JSON.stringify(flightBookingForm),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
           // $('#json-results').html(json);

            var tr;
            var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
            var travelDate = new Date( $("#travel-date").val());

            //document.getElementById("date-value").textContent=travelDate.toLocaleDateString("en-US", options);

            $('#results').empty();

            var results = "<div style=\"display: flex; background: darkgrey; height: 40px; align-items: center\" class=\"border\">" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 100px'></span>" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 325px'>" + travelDate.toLocaleDateString("en-US", options) + "</span>" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'>" + $("#origin").val() + "</span>" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'></span>" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 225px''>" + $("#dest").val() + "</span>" +
                            "<span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'></span>" +
                            "</div>";

            for (var i = 0; i < data.trips.length; i++) {
                results = results.concat("<div style=\"display: flex; background: lightgrey; height: 40px; align-items: center\" class=\"border\">");
                results = results.concat("<span style='width: 100px; align-items: center'> <img src='flight.jpg' style=\"width:25px;height:25px;\"></img></span>");
                results = results.concat("<span style='font: italic bold 16px arial,serif; text-align: center; width: 325px'>");
                results = results.concat(data.trips[i].carrier, " ", data.trips[i].name);
                results = results.concat("</span><span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'>");
                results = results.concat(data.trips[i].departure);
                results = results.concat("</span><span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'>");
                results = results.concat(data.trips[i].duration);
                results = results.concat("</span><span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'>");
                results = results.concat(data.trips[i].arrival);
                results = results.concat("</span><span style='font: italic bold 16px arial,serif; text-align: center; width: 225px'>");

                var price = data.trips[i].travelClassFareMapping[$("#travel-class").val()];
                results = results.concat(price);
                results = results.concat("</span></div>");
            }

            console.log(results);
            $('#results').html(results);

            $("#btn-search").prop("disabled", false);
        },
        error: function (e) {
            var json = "<h4>Ajax Response Error</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);
            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);
        }
    });
});