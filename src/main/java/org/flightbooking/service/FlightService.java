package org.flightbooking.service;

import org.flightbooking.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;

    public FlightService(FlightRepository repository) {
        super();
        this.flightRepository = repository;
    }

    public ArrayList<Trip> getAllFlights() {
        return flightRepository.findAll();
    }


    public ArrayList<TripResultSet> searchFlights(FlightSearch search) {

        ArrayList<Trip> result;
        if(search.getTravelClass() != null)
            result = flightRepository.search(search.getOrigin(), search.getDestination(), search.getNumberOfSeats(),
            search.getTravelDate(), search.getTravelClass());
        else if (search.getTravelDate() != null)
            result = flightRepository.search(search.getOrigin(), search.getDestination(), search.getNumberOfSeats(),
                    search.getTravelDate());
        else
            result = flightRepository.search(search.getOrigin(), search.getDestination(), search.getNumberOfSeats());

        if(result != null) {

            ArrayList<TripResultSet> resultSets = (ArrayList<TripResultSet>) result.stream().map(t -> TripResultSet.getNewTripResult(t,search.getNumberOfSeats() )).collect(Collectors.toList());
            return resultSets;
        }
        return null;
    }

}
