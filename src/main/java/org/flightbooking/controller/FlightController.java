package org.flightbooking.controller;

import org.flightbooking.models.*;
import org.flightbooking.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.OptionalInt;

@RestController
public class FlightController {

    @Autowired
    FlightService service;

    public FlightController(FlightService service) {
        super();
        this.service = service;
    }

    @RequestMapping("/flights") @CrossOrigin
    public ArrayList<Trip> getAllFlight() {
        return service.getAllFlights();
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/search"}) @CrossOrigin
    public SearchFlightResponse searchFlight(@Valid @RequestBody final FlightSearch flightSearch) {

        SearchFlightResponse response = new SearchFlightResponse();

        if(flightSearch.getOrigin().isEmpty() || flightSearch.getDestination().isEmpty())
        {
            response.setMessage("Validation failed");
            response.setTrips(null);
            return response;
        }

        //Look for atleast one seat availability
        if(flightSearch.getNumberOfSeats() == 0)
            flightSearch.setNumberOfSeats(1);

        //If no travel date is specified, default date is tomorrow
        if(flightSearch.getTravelDate() == null)
            flightSearch.setTravelDate(LocalDate.now().plusDays(1));

        ArrayList<TripResultSet> trips = service.searchFlights(flightSearch);
        if(trips.isEmpty())
        {
            response.setMessage("No trips found");
        }
        else
        {
            response.setMessage("Success!");
        }
        response.setTrips(trips);
        return response;
    }

}
