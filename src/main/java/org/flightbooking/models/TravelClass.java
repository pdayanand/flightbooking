package org.flightbooking.models;

public enum TravelClass {
    FirstClass,
    BusinessClass,
    EconomyClass
}
