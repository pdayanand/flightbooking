package org.flightbooking.models;

import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class Trip {
    private int tripId;
    private Route tripRoute;
    private Flight flight;
    private Date    travelDate;
    private Duration flightLength;

    private Map<TravelClass, TravelClassFare> fareDetails;

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public Route getTripRoute() {
        return tripRoute;
    }

    public void setTripRoute(Route tripRoute) {
        this.tripRoute = tripRoute;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Date getTravelDate() {
        return travelDate;
    }

    public Map<TravelClass, TravelClassFare> getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(Map<TravelClass, TravelClassFare> fareDetails) {
        this.fareDetails = fareDetails;
    }

    public void setTravelDate(Date travelDate) {
        this.travelDate = travelDate;
    }

    public Duration getFlightLength() {
        return flightLength;
    }

    public void setFlightLength(Duration flightLength) {
        this.flightLength = flightLength;
    }

    //Check if any of the travel class has seats
    public boolean hasSeats(int seat) {

        for (TravelClassFare t: fareDetails.values()) {
            if(t.getRemainingSeats() >= seat)
                return true;
        }
        return false;
    }

    public boolean isSameDate(LocalDate date) {
        Calendar c = Calendar.getInstance();
        c.setTime(travelDate);
        if(c.get(Calendar.YEAR) == date.getYear() &&
                c.get(Calendar.DAY_OF_YEAR) == date.getDayOfYear() )
            return true;

        return false;
    }

    public boolean hasSeatsInTravelClass(int seats, TravelClass travelClass){
        LocalDate localDate = travelDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        if(travelClass == TravelClass.FirstClass)
            if(localDate.isAfter(LocalDate.now().plusDays(10)))
                return false;

        if(fareDetails.containsKey(travelClass) && fareDetails.get(travelClass).getRemainingSeats() > seats)
            return true;

        return  false;
    }
}
