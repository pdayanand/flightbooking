package org.flightbooking.models;

import java.util.Map;

public class Flight {
    private int flightId;
    private String name;
    private String carrier;
    private AirPlaneModel model;

    private int economySeats;
    private int businessSeats;
    private int firstSeats;

    //Constructor used by Repository class to populate test data
    public Flight(int flightId, String name, String carrier, AirPlaneModel model, int economySeats, int businessSeats, int firstSeats) {
        this.flightId = flightId;
        this.name = name;
        this.carrier = carrier;
        this.model = model;
        this.economySeats = economySeats;
        this.businessSeats = businessSeats;
        this.firstSeats = firstSeats;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public AirPlaneModel getModel() {
        return model;
    }

    public void setModel(AirPlaneModel model) {
        this.model = model;
    }

    public int getEconomySeats() {
        return economySeats;
    }

    public void setEconomySeats(int economySeats) {
        this.economySeats = economySeats;
    }

    public int getBusinessSeats() {
        return businessSeats;
    }

    public void setBusinessSeats(int businessSeats) {
        this.businessSeats = businessSeats;
    }

    public int getFirstSeats() {
        return firstSeats;
    }

    public void setFirstSeats(int firstSeats) {
        this.firstSeats = firstSeats;
    }
}
