package org.flightbooking.models;


import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


@Repository
public class FlightRepository {

    private Map<Integer, Trip> repository;
    private int temp = 0;

    public FlightRepository() {
        this.repository = new HashMap<>();
        populateTrips(); //Fill test data
    }

    private ArrayList<Route> populateRoutes() {
        //Creates 9 routes - Ban to Mum, Ban to Pune, Ban to Delhi, Chn to Mumbai, etc
        ArrayList<Route> routes = new ArrayList<>();
        String[] origins = {"Bangalore", "Chennai", "Hyderabad"};
        String[] destinations = {"Mumbai", "Pune", "Delhi"};

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Route r = new Route((i + 1) * (j + 1), origins[i], destinations[j]);
                routes.add(r);
            }
        }
        return routes;
    }

    private ArrayList<Flight> populateFlights() {
        //Creates 9 Flight objects
        // IN 1, IN 2, IN 3, EM 1, EM 2, EM 3, etc
        // IN 1, EM 1, JA 1 are  BOEING_777_200LR, (has FC, BC & EC)
        // IN 2, EM 2, JA 2 are  AIRBUS_A319_V2, (has EC)
        // IN 3, EM 3, JA 3 are  AIRBUS_A321 (has EC & BC)
        ArrayList<Flight> flights = new ArrayList<>();
        String[] name = {"IN ", "EM ", "JA "};
        String[] carrier = {"Indigo", "Emirates", "Jet airways"};
        int[] economySeats = {195, 144, 152};
        int[] businessSeats = {35, 0, 20};
        int[] firstSeats = {8, 0, 0};

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int id = j + 1;
                Flight f = new Flight(id,
                        name[i] + id,
                        carrier[i],
                        AirPlaneModel.values()[j],
                        economySeats[j],
                        businessSeats[j],
                        firstSeats[j]);
                flights.add(f);
            }
        }
        return flights;
    }

    private void populateTrips() {

        ArrayList<Route> routes = populateRoutes(); //Total 9 routes
        ArrayList<Flight> flights = populateFlights(); // Total 9 Aeroplanes


        // Flight name      Route           Flight
        // IN 1             Ban to Mum      BOEING_777_200LR
        // IN 2             Ban to Pune     AIRBUS_A319_V2
        // IN 3             Ban to Del      AIRBUS_A321
        // EM 1             Chn to Mum      BOEING_777_200LR
        // EM 2             Chn to Pune     AIRBUS_A319_V2
        // EM 3             Chn to Del      AIRBUS_A321
        // JA 1             Hyd to Mum      BOEING_777_200LR
        // JA 2             Hyd to Pune     AIRBUS_A319_V2
        // JA 3             Hyd to Del      AIRBUS_A321
        // Daily trips => 6.15, 10.15, 14.15 & 18.15
        // Total trips 4 (daily trips) * 9 (routes) * 60 days = 2160
        int idCounter = 0;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        for (int i = 0; i < 60; i++) {
            for (int j = 0; j < 9; j++) {

                Trip trip = new Trip();
                trip.setTripId(++idCounter);
                trip.setFlight(flights.get(j));
                trip.setTripRoute(routes.get(j));
                trip.setFlightLength(Duration.parse("P0DT2H30M"));
                c.set(Calendar.HOUR_OF_DAY, 6);
                c.set(Calendar.MINUTE, 15);
                c.set(Calendar.SECOND, 0);
                trip.setFareDetails(getTravelClassFare(trip.getFlight().getModel()));
                trip.setTravelDate(c.getTime());

                repository.put(idCounter, trip);

                trip = new Trip();
                trip.setTripId(++idCounter);
                trip.setFlight(flights.get(j));
                trip.setTripRoute(routes.get(j));
                trip.setFlightLength(Duration.parse("P0DT2H30M"));
                c.set(Calendar.HOUR_OF_DAY, 10);
                c.set(Calendar.MINUTE, 15);
                c.set(Calendar.SECOND, 0);
                trip.setFareDetails(getTravelClassFare(trip.getFlight().getModel()));
                trip.setTravelDate(c.getTime());

                repository.put(idCounter, trip);

                trip = new Trip();
                trip.setTripId(++idCounter);
                trip.setFlight(flights.get(j));
                trip.setTripRoute(routes.get(j));
                trip.setFlightLength(Duration.parse("P0DT2H30M"));
                c.set(Calendar.HOUR_OF_DAY, 14);
                c.set(Calendar.MINUTE, 15);
                c.set(Calendar.SECOND, 0);
                trip.setFareDetails(getTravelClassFare(trip.getFlight().getModel()));
                trip.setTravelDate(c.getTime());

                repository.put(idCounter, trip);

                trip = new Trip();
                trip.setTripId(++idCounter);
                trip.setFlight(flights.get(j));
                trip.setTripRoute(routes.get(j));
                trip.setFlightLength(Duration.parse("P0DT2H30M"));
                c.set(Calendar.HOUR_OF_DAY, 18);
                c.set(Calendar.MINUTE, 15);
                c.set(Calendar.SECOND, 0);
                trip.setFareDetails(getTravelClassFare(trip.getFlight().getModel()));
                trip.setTravelDate(c.getTime());

                repository.put(idCounter, trip);

            }
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
    }


    private Map<TravelClass, TravelClassFare> getTravelClassFare(AirPlaneModel model) {
        switch (model) {
            case BOEING_777_200LR:
                return getBoeingTravelClassFare();
            case AIRBUS_A319_V2:
                return getAirbusA319TravelClassFare();
            case AIRBUS_A321:
                return getAirbusA321TravelClassFare();
        }
        return null;
    }


    private Map<TravelClass, TravelClassFare> getBoeingTravelClassFare() {
        Map<TravelClass, TravelClassFare> result = new HashMap<>();

        TravelClassFare boeing777FC = new TravelClassFare();
        TravelClassFare boeing777BC = new TravelClassFare();
        TravelClassFare boeing777EC = new TravelClassFare();


        int t = temp ++;
        int remSeats = 144;
        if(t % 3 == 1)
        {
            remSeats= 135;
            boeing777FC.setBasePrice(25000);
            boeing777BC.setBasePrice(10000);
            boeing777EC.setBasePrice(7000);

        }
        else if (t%3 == 2){
            remSeats = 70;
            boeing777FC.setBasePrice(15000);
            boeing777BC.setBasePrice(80000);
            boeing777EC.setBasePrice(3000);
        }
        else {
            remSeats = 5;
            boeing777FC.setBasePrice(20000);
            boeing777BC.setBasePrice(13000);
            boeing777EC.setBasePrice(6000);
        }

        boeing777FC.setTravelClass(TravelClass.FirstClass);

        boeing777FC.setRemainingSeats(8);



        boeing777BC.setTravelClass(TravelClass.BusinessClass);

        boeing777BC.setRemainingSeats(35);


        boeing777EC.setTravelClass(TravelClass.EconomyClass);

        boeing777EC.setRemainingSeats(remSeats);

        result.put(TravelClass.FirstClass, boeing777FC);
        result.put(TravelClass.BusinessClass, boeing777BC);
        result.put(TravelClass.EconomyClass, boeing777EC);

        return result;
    }

    private Map<TravelClass, TravelClassFare> getAirbusA319TravelClassFare() {
        Map<TravelClass, TravelClassFare> result = new HashMap<>();

        TravelClassFare airbusA319EC = new TravelClassFare();
        airbusA319EC.setTravelClass(TravelClass.EconomyClass);

        int t = temp ++;
        int remSeats = 144;
        if(t % 3 == 1)
        {
            remSeats= 135;
        }
        else if (t%3 == 2){
            remSeats = 70;
        }
        else {
            remSeats = 5;
        }

        airbusA319EC.setRemainingSeats(remSeats);
        airbusA319EC.setBasePrice(4000);


        result.put(TravelClass.EconomyClass, airbusA319EC);

        return result;
    }

    private Map<TravelClass, TravelClassFare> getAirbusA321TravelClassFare() {
        Map<TravelClass, TravelClassFare> result = new HashMap<>();

        TravelClassFare airbusA321EC = new TravelClassFare();
        airbusA321EC.setTravelClass(TravelClass.EconomyClass);

        int t = temp ++;
        int remSeats = 144;
        if(t % 3 == 1)
        {
            remSeats= 135;
        }
        else if (t%3 == 2){
            remSeats = 70;
        }
        else {
            remSeats = 5;
        }

        airbusA321EC.setRemainingSeats(remSeats);
        airbusA321EC.setBasePrice(5000);


        TravelClassFare airbusA321BC = new TravelClassFare();
        airbusA321BC.setTravelClass(TravelClass.BusinessClass);

        airbusA321BC.setRemainingSeats(20);
        airbusA321BC.setBasePrice(10000);


        result.put(TravelClass.BusinessClass, airbusA321BC);
        result.put(TravelClass.EconomyClass, airbusA321EC);

        return result;
    }


    public ArrayList<Trip> findAll() {
        return new ArrayList<Trip>(repository.values());
    }


    public ArrayList<Trip> search(String origin, String destination, int seatsAvailability) {
        Collection<Trip> trips = repository.values();
        ArrayList<Trip> result ;


        result = (ArrayList<Trip>) trips.stream().filter(t -> t.getTripRoute().getOrigin().equalsIgnoreCase(origin) &&
                t.getTripRoute().getDestination().equalsIgnoreCase(destination) &&
                t.hasSeats(seatsAvailability)).collect(Collectors.toList());

        return result;
    }

    public ArrayList<Trip> search(String origin, String destination, int seatsAvailability, LocalDate travelDate) {

        Collection<Trip> trips = repository.values();
        ArrayList<Trip> result ;


        result = (ArrayList<Trip>) trips.stream().filter(t -> t.getTripRoute().getOrigin().equalsIgnoreCase(origin) &&
                t.getTripRoute().getDestination().equalsIgnoreCase(destination) &&
                t.hasSeats(seatsAvailability) &&
                t.isSameDate(travelDate)
        ).collect(Collectors.toList());

        return result;
    }

    public ArrayList<Trip> search(String origin, String destination, int seatsAvailability, LocalDate travelDate, TravelClass travelClass) {
        Collection<Trip> trips = repository.values();
        ArrayList<Trip> result ;


        result = (ArrayList<Trip>) trips.stream().filter(t -> t.getTripRoute().getOrigin().equalsIgnoreCase(origin) &&
                t.getTripRoute().getDestination().equalsIgnoreCase(destination) &&
                t.hasSeatsInTravelClass(seatsAvailability, travelClass) &&
                t.isSameDate(travelDate)
        ).collect(Collectors.toList());

        return result;
    }

}
