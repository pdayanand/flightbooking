package org.flightbooking.models;

import java.util.ArrayList;

public class SearchFlightResponse {

    private String message;
    ArrayList<TripResultSet> trips;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TripResultSet> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<TripResultSet> trips) {
        this.trips = trips;
    }
}
