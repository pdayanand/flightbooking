package org.flightbooking.models;

import org.hibernate.validator.internal.util.logging.formatter.DurationFormatter;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TripResultSet {
    private int tripId;

    private String origin;
    private String destination;

    private String name;
    private String carrier;
    private AirPlaneModel model;

    private String travelDate;
    private String departure;
    private String arrival;
    private String duration;



    private Map<TravelClass, Integer> travelClassFareMapping = new HashMap<>() ;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public AirPlaneModel getModel() {
        return model;
    }

    public void setModel(AirPlaneModel model) {
        this.model = model;
    }

    public String getTravelDate() {
        return travelDate;
    }

    public void setTravelDate(String travelDate) {
        this.travelDate = travelDate;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public Map<TravelClass, Integer> getTravelClassFareMapping() {
        return travelClassFareMapping;
    }

    public void setTravelClassFareMapping(Map<TravelClass, Integer> travelClassFareMapping) {
        this.travelClassFareMapping = travelClassFareMapping;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void addFare(TravelClass travelClass, Integer fare)
    {
        travelClassFareMapping.put(travelClass, fare);
    }

    public static TripResultSet getNewTripResult(Trip t, int seats) {
        TripResultSet resultSet = new TripResultSet();
        resultSet.tripId = t.getTripId();
        resultSet.origin = t.getTripRoute().getOrigin();
        resultSet.destination = t.getTripRoute().getDestination();

        resultSet.name = t.getFlight().getName();
        resultSet.carrier = t.getFlight().getCarrier();
        resultSet.model = t.getFlight().getModel();



        Calendar c = Calendar.getInstance();
        c.setTime(t.getTravelDate());

        resultSet.travelDate = (new SimpleDateFormat("dd/MM/yyyy").format(c.getTime()));

        resultSet.departure = (new SimpleDateFormat("HH:mm").format(c.getTime()));


        c.add(Calendar.MINUTE, (int)t.getFlightLength().toMinutes());
        resultSet.arrival =  (new SimpleDateFormat("HH:mm").format(c.getTime()));


        resultSet.duration =  new DurationFormatter(t.getFlightLength()).toString();
        resultSet.travelClassFareMapping = t.getFareDetails().values().stream()
                .collect(Collectors.toMap(TravelClassFare::getTravelClass, fare -> getFare(fare, t, seats)));

        return  resultSet;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    private static Integer getFare(TravelClassFare fare, Trip trip, int seats) {

        switch(fare.getTravelClass()) {
            case EconomyClass:
                float totalSeats = (float)trip.getFlight().getEconomySeats();
                float remainingSeats = (float)fare.getRemainingSeats();
                float percentage = (remainingSeats/totalSeats * 100);
                if(percentage > 60) {
                    return fare.getBasePrice() * seats;
                } else if (percentage > 10)
                    return (int) Math.ceil(fare.getBasePrice() * 1.3 * seats);
                else
                    return (int) Math.ceil(fare.getBasePrice() * 1.6 * seats);


            case BusinessClass:
                Calendar cBusinessClass = Calendar.getInstance();
                cBusinessClass.setTime(trip.getTravelDate());
                int dayOfWeek = cBusinessClass.get(Calendar.DAY_OF_WEEK);
                if(dayOfWeek == 1 || dayOfWeek == 2 || dayOfWeek == 6 )
                    return (int) Math.ceil(fare.getBasePrice() * 1.4 * seats);
                return fare.getBasePrice() * seats;

            case FirstClass:

                LocalDate firstClassWindowStart = LocalDate.now();
                LocalDate firstClassWindowEnd = LocalDate.now().plusDays(10);
                float multiplier = (float) 1.0;

                LocalDate travelDate = trip.getTravelDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                if((firstClassWindowStart.isBefore(travelDate) || firstClassWindowStart.isEqual(travelDate)) && firstClassWindowEnd.isAfter(travelDate)) {
                    multiplier += (float)Period.between(travelDate, firstClassWindowEnd).getDays() * 0.1;
                }

                return (int) Math.ceil(fare.getBasePrice() * seats * multiplier);
        }
        return fare.getBasePrice() * seats;
    }
}
