package org.flightbooking.models;

public enum AirPlaneModel {
    BOEING_777_200LR,
    AIRBUS_A319_V2,
    AIRBUS_A321
}
